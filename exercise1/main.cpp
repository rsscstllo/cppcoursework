//
//  main.cpp
//  fibonacci
//
//  Ross Castillo
//  February 16, 2015
//
//  This program implements the fibonacci sequence in three ways to calculate the n-th fibonacci
//  number and measures the running time for each implementation.

#include <iostream>
#include <ctime>

using namespace std;

// Function declarations
int fibonacci_recursive(int n);
int fibonacci_iterative(int n);
int fibonacci_dynamic(int n);

// Main function
int main(int argc, const char * argv[]) {
  
  // Create a variable to represent the user's input and then prompt the user for input.
  int n;
  cout << "Enter a number: ";
  cin >> n;
  cout << endl;
  
  // Varibles to keep track of the running time for each approach.
  // All type int, so as long as you cast one, they will all be calculated as doubles.
  // Can't just cast outstide of it all because the equation would calculated with ints first and
  // then be casted to double but it will have already been int by then, so double wouldn't make a
  // difference.
  clock_t start_time;
  clock_t finish_time;
  double elapsed_time;
  
  cout << "RECURSIVE RESULT: " << endl;
  cout << "fibonacci(" << n << ") \t\t= ";
  start_time = clock();
  cout << fibonacci_recursive(n) << endl;
  finish_time = clock();
  elapsed_time = ((double)finish_time - start_time) / CLOCKS_PER_SEC;
  cout << "time complexity \t= " << elapsed_time << endl << endl;
  
  cout << "DYNAMIC RESULT: " << endl;
  cout << "fibonacci(" << n << ") \t\t= ";
  start_time = clock();
  cout << fibonacci_dynamic(n) << endl;
  finish_time = clock();
  elapsed_time = ((double)finish_time - start_time) / CLOCKS_PER_SEC;
  cout << "time complexity \t= " << elapsed_time << endl << endl;
  
  cout << "Iterative RESULT: " << endl;
  cout << "fibonacci(" << n << ") \t\t= ";
  start_time = clock();
  cout << fibonacci_iterative(n) << endl;
  finish_time = clock();
  elapsed_time = ((double)finish_time - start_time) / CLOCKS_PER_SEC;
  cout << "time complexity \t= " << elapsed_time << endl << endl;
  
  return 0;
}

// Recursive solution for calculating the n-th fibonacci number
int fibonacci_recursive(int n) {
  int answer;
  
  // Base cases
  if (n == 0) return 0;
  if (n == 1) return 1;
  
  // Recursive step
  answer = fibonacci_recursive(n - 1) + fibonacci_recursive(n - 2);
  
  return answer;
}

// Iterative solution for calculating the n-th fibonacci number
int fibonacci_iterative(int n) {
  int a = 1;
  int b = 1;
  
  for (int i = 2; i < n; i++) {
    a = a + b;
    swap(a, b);
  }
  
  return b;
}

// Dynamic programming solution for calculating the n-th fibonacci number
int fibonacci_dynamic(int n) {
  int* answer = new int[n + 1];
  
  // Base cases
  answer[0] = 0;
  answer[1] = 1;
  
  for (int i = 2; i <= n; i++) {
    answer[i] = answer[i - 1] + answer[i - 2];
  }
  
  return answer[n];
}
