//
//  main.cpp
//  exercise3
//
//  Created by Ross Castillo on 3/24/15.
//  Copyright (c) 2015 Ross Castillo. All rights reserved.
//

#include <iostream>

// Each index in the hash table is a bucket and each bucket can contain a list of items chained
// together with pointers. This class represents each item in a given bucket for each index of the
// hash table. There are two instance variables, one for the string that's inserted into the hash
// table and one that points to the next item in the bucket. The constructor sets the string value
// to be what the inserted string is and the pointer to the next item to NULL. This class is
// essentially a linked list. So, in other words, each bucket in the hash table has a linked list.
class BucketListItem {
private:
  std::string value_;
  BucketListItem* next_item_ptr_;
public:
  BucketListItem(std::string value) {
    value_ = value;
    next_item_ptr_ = NULL;
  }
  std::string get_value() {
    return value_;
  }
  void set_value(int value) {
    value_ = value;
  }
  BucketListItem* get_next() {
    return next_item_ptr_;
  }
  void set_next(BucketListItem* next) {
    next_item_ptr_ = next;
  }
};

// This class represents the actual hash table. There are two instance variables, one that
// represents the actual table as 2-D array composed of BucketListItem's and one for the size of
// the hash table.
//
// The constructor initializes the size of the hash table, assigns the first dimension of the array
// to be that size, and then sets each BucketListItem at each index of the hash table to NULL.
//
// The get_index() function returns an index value of where the inserted string is placed in the
// hash table. It calculates the sum of the string based on its ascii values of each character and
// then mods that sum with size of the table to return a valid index value that is in range.
//
// The search function gets the index of the string being searched and makes a new BucketListItem
// to represent the BucketListItem at that index. Then, iterates though all of the valid items in
// that bucket until the identical string is found.
//
// The insert function finds the index for the string to be inserted and inserts it at that index,
// or bucket, to be the last item in that bucket's list of items.
// The destructor then deletes the allcoated memory on the heap for the table. It deletes each
// item in each bucket of the hash table that is not NULL.
class MyHashTable {
private:
  BucketListItem **table_;
  int table_size_;
public:
  MyHashTable(int table_size) {
    table_size_ = table_size;
    table_ = new BucketListItem*[table_size];
    for (int i = 0; i < table_size; i++)
      table_[i] = NULL;
  }
  int get_index(std::string key, int table_size) {
    int index = 0;
    for (int i = 0; i < (int)key.length(); i++)
      index += key[i];
    return index % table_size;
  }
  void search(std::string value) {
    int index = get_index(value, table_size_);
    if (table_[index] == NULL)
      std::cout << "NO" << std::endl;
    else {
      BucketListItem* item = table_[index];
      while (item != NULL && item->get_value() != value)
        item = item->get_next();
      if (item == NULL)
        std::cout << "NO" << std::endl;
      else
        std::cout << "YES" << std::endl;
    }
  }
  void insert(std::string value) {
    int index = get_index(value, table_size_);
    if (table_[index] == NULL)
      table_[index] = new BucketListItem(value);
    else {
      BucketListItem* item = table_[index];
      while (item->get_next() != NULL)
        item = item->get_next();
      item->set_next(new BucketListItem(value));
    }
  }
  ~MyHashTable() {
    for (int i = 0; i < table_size_; i++) {
      if (table_[i] != NULL) {
        BucketListItem* prev_item = NULL;
        BucketListItem* next_item = table_[i];
        while (next_item != NULL) {
          prev_item = next_item;
          next_item = next_item->get_next();
          delete prev_item;
        }
      }
    }
    delete[] table_;
  }
};

// The main function reads in the table size, creates a hash table with that size, and then calls
// the appropriate functions based on user input.
int main(int argc, const char *argv[]) {
  int table_size;
  std::cin >> table_size;
  MyHashTable* ht = new MyHashTable(table_size);
  int op = 0;
  while (op < 3) {
    std::string input;
    std::cin >> op;
    switch (op) {
      case 0:
        op = 3;
      case 1:
        std::cin >> input;
        ht->insert(input);
        break;
      case 2:
        std::cin >> input;
        ht->search(input);
        break;
    }
  }
  delete ht;
  return 0;
}
