//
//  sDeque_main.cpp
//  project1a
//  
//  Created by Ross Castillo on 2/11/15.
//  Copyright (c) 2015 Ross Castillo. All rights reserved.
//

#include "sDeque.h"
#include <iostream>
#include <string>

int main(int argc, const char * argv[]) {
  int op = 0;
  std::string input;
  Deque *DQ = new Deque();
  
//  // add 8 elemtns
//  DQ->push_front("a");
//  DQ->push_front("b");
//  DQ->push_front("c");
//  DQ->push_front("d");
//  DQ->push_front("e");
//  DQ->push_front("f");
//  DQ->push_front("g");
//  DQ->push_front("h");
//  std::cout << DQ->toStr() << std::endl;
//  
//  // add 2 more for 10 – Double queue
//  DQ->push_front("i");
//  DQ->push_front("j");
//  std::cout << DQ->toStr() << std::endl;
//  
//  // add 6 more for 16
//  DQ->push_front("k");
//  DQ->push_front("l");
//  DQ->push_front("m");
//  DQ->push_front("n");
//  DQ->push_front("o");
//  DQ->push_front("p");
//  std::cout << DQ->toStr() << std::endl;
//  
//  // add 2 more for 18 – Double queue
//  DQ->push_front("q");
//  DQ->push_front("r");
//  std::cout << DQ->toStr() << std::endl;
  
  while (op < 5) {
    std::cin >> op;
    switch(op) {
      case 0:
        std::cin >> input;
        DQ->push_front(input);
        break;
      case 1:
        std::cin >> input;
        DQ->push_back(input);
        break;
      case 2:
        DQ->pop_front();
        break;
      case 3:
        DQ->pop_back();
        break;
      case 4:
        std::cout << DQ->toStr();
        break;
    }
  }
  return 0;
}
