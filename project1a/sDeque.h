//
//  sDeque.h
//  project1a
//
//  Created by Ross Castillo on 2/11/15.
//  Copyright (c) 2015 Ross Castillo. All rights reserved.
//

#ifndef SDEQUE_H_
#define SDEQUE_H_

#include <string>
#include <sstream>
#include <iostream>
#include <stdlib.h>  // exit (EXIT_FAILURE);

class Deque {
private:
  // Instance variables
  std::string* container_;  // A pointer to a string object of which queue will be comprised.
  int container_capacity_;  // How many elements the current queue can hold.
  int num_elements_;        // Number of elements currently in the queue.
  int head_index_;          // Queue's starting index.
  int tail_index_;          // Queue's ending index.
  
  // Private helper function for the push functions that doubles the size of the queue when an
  // element is pushed to a full queue. A temporary array is created that's twice the size of the
  // old one. The elements from the old array are then copied to the new one in the correct order.
  // The head index will not be negative here, so it's okay to use the mod operator with it when it
  // goes past the size of the original queue. Set the new tail index to be the last element in the
  // queue, move the head index back to the first element in the queue, delete the old array, and
  // then set it equal to the temporary one to create the new queue. The temporary array doesn't
  // have to be deleted – memory will override it next time the function is called.
  void helper_grow() {
    std::string* temp_container_ = new std::string[container_capacity_ * 2];
    
    for (int i = 0; i != num_elements_; ++i)
      temp_container_[i] = container_[(head_index_ + i) % container_capacity_];
    
    tail_index_ = num_elements_ - 1;
    head_index_ = 0;
    container_capacity_ *= 2;
    
    delete[] container_;
    container_ = temp_container_;
  }
  
  // Private helper function for the pop functions that decreases the size of the array by half. It
  // creates a temporary array to replace the old one and transfers its elements starting at the
  // head index incrementing up. The old queue is deleted and then replaced with the new one that's
  // half the size. The head and tail indices are then properly reset to fit the new samller queue.
  void helper_shrink() {
    std::string* temp_container_ = new std::string[container_capacity_ / 2];
    
    for (int i = 0; i != num_elements_; ++i)
      temp_container_[i] = container_[(head_index_ + i) % container_capacity_];
    
    delete[] container_;
    container_ = temp_container_;
    container_capacity_ /= 2;
    head_index_ = 0;
    tail_index_ = num_elements_ - 1;
  }
  
  // Tells whether the queue is full or not by checking if there's an equal amount of elements
  // stored as there are capable.
  bool is_full() {
    return (num_elements_ == container_capacity_);
  }
  
public:
  // Constructor that is called every time an object of class Deque is created. It creates and
  // initializes a new array of 8 pointers to string objects, sets the capacity to 8, and
  // appropriately sets the indicies to increment correctly in subsequent functions.
  Deque() {
    container_ = new std::string[8];
    container_capacity_ = 8;
    num_elements_ = 0;
    head_index_ = 0;
    tail_index_ = -1;
  }
  
  // Inserts an element at the front of the queue. First check's if the queue is already full and
  // grows it as necessary, increments the number of elements since a new element is being added,
  // decrements the head and adds it to queue's capacity if its negative to allow for wrap-around
  // functionality and avoid quadratic time complexity of indexing through the array. The item is
  // then placed where the new head index is.
  void push_front(std::string item) {
    if (is_full())
      helper_grow();
    
    // Reset the indices if the queue is empty, just in case either is off.
    if (empty()) {
      head_index_ = 0;
      tail_index_ = -1;
    }
    
    ++num_elements_;
    --head_index_;
    
    if (head_index_ < 0)
      head_index_ += container_capacity_;
    
    container_[head_index_] = item;
  }
  
  // Inserts an element at the back of the queue. First checks if the queue is already full and
  // grows it as necessary, increments the number of elements since a new element is being added,
  // increments the tail index to move down up in the queue and place the item there.
  void push_back(std::string item) {
    if (is_full())
      helper_grow();
    
    // Reset the indices if the queue is empty, just in case either is off.
    if (empty()) {
      head_index_ = 0;
      tail_index_ = -1;
    }
    
    ++num_elements_;
    ++tail_index_;
    
    container_[tail_index_] = item;
  }
  
  // Removes and returns the element at the front of the queue. First checks if the queue is empty
  // and exits the program indicating a failure. Also checks if the queue is only a quarter full
  // and at greater than 8 and appropriately calls the shrink function. After checking the two
  // conditions, the number of elements are decremented by 1 since one element is being popped off
  // and the head index is incremeneted by one since the first element is now gone. If the head
  // index exceeds the queue, its subtracted from the queue's capacity to wrap back around to front
  // of the array. The string at the original position is then returned.
  std::string pop_front() {
    if (empty())
      exit (EXIT_FAILURE);

    if (num_elements_ == (container_capacity_) / 4 && (container_capacity_ > 8))
      helper_shrink();
    
    --num_elements_;
    ++head_index_;
    
    if (head_index_ > container_capacity_)
      head_index_ -= container_capacity_;
    
    return container_[(head_index_ - 1)];
  }
  
  // Removes and returns the element at the back of the queue. Checks necessary conditions to exit
  // the program or call the shrink function, decrements the number of elements since an element is
  // getting popped off, and decrements the tail index by 1 since the element being popped off is
  // popped off from the back where the current tail index is. The tail index is added to the
  // queue's capacity if it's negative for wrap-around functionality.
  std::string pop_back() {
    if (empty())
      exit (EXIT_FAILURE);
    
    if (num_elements_ == (container_capacity_) / 4 && (container_capacity_ > 8))
      helper_shrink();
    
    --num_elements_;
    --tail_index_;
    
    if (tail_index_ < -1)  // Since tail index starts at -1.
      tail_index_ += container_capacity_;
    
    return container_[(tail_index_ + 1)];
  }
  
  // Returns the number of elements in the queue.
  int size() {
    return container_capacity_;
  }
  
  // Tells whether the queue is empty or not and returns true if the number of elements is indeed 0.
  bool empty() {
    return (num_elements_ == 0);
  }
  
  // Puts the contents of the queue from the front to the back into a return string with each string
  // item followed by a new line. Iterate through the queue for how many elements are currently in
  // it and then choose the elements starting from the head index up to the last element. If the
  // head index exceeds the length of the queue, it's then modded with the queue size to wrap-around
  // back to the first index of the queue when needed. Use the \n for a new line as implied in the
  // directions to avoid possible errors from clearing the buffer with endl.
  std::string toStr() {    
    std::stringstream ss;
    for (int i = 0; i != num_elements_; ++i)
      ss << container_[((head_index_ + i) % container_capacity_)] << "\n";
    return ss.str();
  }
  
  // Destructor to delete the memory allocated to the array of pointers to string objects.
  ~Deque() {
    delete[] container_;
  }
};

#endif // SDEQUE_H_
