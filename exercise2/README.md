# README #

### About This Repository ###

* Linear Search vs. Binary Search with compared run-times.  

### How To Set Up ###

* Download the project, open Terminal, and type the following commands once in the home directory of the 
project:  
1. `make`  
2. `g++ input-gen.cpp -o generator`  
3. `./generator 1000000 100 > input-1m.in`  
4. `./search < "input-1m.in"`  
