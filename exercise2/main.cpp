//
//  main.cpp
//  exercise2
//
//  Created by Ross Castillo on 3/19/15.
//  Copyright (c) 2015 Ross Castillo. All rights reserved.
//

#include <ctime>        // clock_t
#include <iostream>     // cin, cout
#include <algorithm>    // sort()

// Function declarations.
void LinearSearch(int ary_elements[], int NE, int key);
void BinarySearch(int ary_elements[], int lo, int hi, int key);


int main(int argc, const char *argv[]) {
  int NE;  // Number of elements.
  int NS;  // Number of elements to search.
  
  std::cin >> NE >> NS;  // Read in values.
  
  
  int ary_elements[NE];         // Array to hold all of the elements.
  int ary_elements_search[NS];  // Array to hold the elements to be searched.
  
  
  // Read in each element.
  for (int i = 0; i < NE; ++i)
    std::cin >> ary_elements[i];
  
  
  // Sort the elements.
  std::sort(ary_elements, ary_elements + NE);
  
  
  // Read in the elements to be searched.
  for (int i = 0; i < NS; ++i)
    std::cin >> ary_elements_search[i];
  
  
  // Variables to keep track of running time for each search function.
  clock_t start_time;
  clock_t finish_time;
  double elapsed_time;
  
  
  // Linear search.
  std::cout << "\nLinear Search:" << std::endl;
  start_time = clock();
  
  for (int i = 0; i < NS; ++i)
    LinearSearch(ary_elements, NE, ary_elements_search[i]);
  
  finish_time = clock();
  elapsed_time = ((double)finish_time - start_time) / CLOCKS_PER_SEC;
  std::cout << "TIME: " << elapsed_time << std::endl << std::endl;
  
  
  // Binary search.
  std::cout << "Binary Search:" << std::endl;
  start_time = clock();
  
  for (int i = 0; i < NS; ++i) {
    int lo = 0, hi = NE;
    BinarySearch(ary_elements, lo, hi, ary_elements_search[i]);
  }
  
  finish_time = clock();
  elapsed_time = ((double)finish_time - start_time) / CLOCKS_PER_SEC;
  std::cout << "TIME: " << elapsed_time << std::endl << std::endl;
  
  
  return 0;
}

// Iterate through all the elements in the array. If the key matches an element, print "Yes"
// to the console to indicate success and then break from the loop to end the function. If the key
// is not found after iterating through the whole array, print "No" to the console indicating a
// falure, or that the key was not found.
void LinearSearch(int ary_elements[], int NE, int key) {
  for (int i = 0; i <= NE; ++i) {
    if (key == ary_elements[i]) {
      std::cout << "Yes" << std::endl;
      break;
    } else if (i == NE)
      std::cout << "No" << std::endl;
  }
}

// Create an integer to represent the midpoint of the array. The midpoint will be the floor value,
// meaning the number closer to zero if the array has an even size without an exact middle. Iterate
// though the array so long as the key is not equal to the midpoint value and hi is greater than or
// equal to lo. If the midpoint is greater than the key, then make the new hi value one under the
// midpoint to now only search the bottom half of the array. Otherwise, if the key is greater than
// the midpoint, make the lo value one above the midpoint and only search the top half of the array.
// Then, reset the new midopoint value before checking again. Now, if the lo and hi indicies are
// equal print out "Yes" to the console to indicate a successful find and if not, print out "No" to
// indicate failure or no find.
void BinarySearch(int ary_elements[], int lo, int hi, int key) {
  int mi = (lo + hi) / 2;
  while ((ary_elements[mi] != key) && (lo <= hi)) {
    if (ary_elements[mi] > key)
      hi = mi - 1;
    else
      lo = mi + 1;
    mi = (lo + hi) / 2;
  }
  if (lo <= hi)
    std::cout << "Yes" << std::endl;
  else
    std::cout << "No" << std::endl;
}
