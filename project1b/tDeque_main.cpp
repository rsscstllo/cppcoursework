//
//  tDeque_main.cpp
//  project1b
//
//  Created by Ross Castillo on 2/15/15.
//  Copyright (c) 2015 Ross Castillo. All rights reserved.
//

#include "tDeque.h"

// Function declaration for main()
template<typename T> void test(T s);

int main(int argc, char **argv) {
  // int op = 0;
  // std::string input;
  int type;
  std::cin >> type;
  std::string s = "tDeque";
  
  switch(type) {
    case 0:
      test(s);
      break;
      
    case 1:
      test(3.2);
      break;
      
    case 2:
      test(1);
      break;
      
    default:
      return 1;
  }
  return 0;
}

template<typename T> void test(T s) {
  Deque<T> *DQ = new Deque<T>();
  
  T input;
  int op = 0;
  
  while (op < 7) {
    std::cin >> op;
    
    switch(op) {
      case 0:
        std::cin >> input;
        try {
          DQ->push_front(input);
        } catch (std::exception e) {
          std::cout << "Out of Memory Exception!" << std::endl;
        }
        break;
        
      case 1:
        std::cin >> input;
        try {
          DQ->push_back(input);
        } catch (std::exception e) {
          std::cout << "Out of Memory Exception!" << std::endl;
        }
        break;
        
      case 2:
        try {
          std::cout << DQ->pop_front() << std::endl;
        } catch (std::exception e) {
          std::cout << "Caught Exception for empty stack!" << std::endl;
        }
        break;
        
      case 3:
        try {
          std::cout << DQ->pop_back() << std::endl;
        } catch (std::exception e) {
          std::cout << "Caught Exception for empty stack!" << std::endl;
        }
        break;
        
      case 4:
        std::cout << DQ->toStr();
        break;
        
      case 5:
        std::cout << DQ->size() << std::endl;
        break;
        
      case 6:
        std::cout << DQ->empty() << std::endl;
        break;
    }
  }
}
