//
//  tDeque.h
//  project1b
//
//  Created by Ross Castillo on 2/15/15.
//  Copyright (c) 2015 Ross Castillo. All rights reserved.
//

#ifndef TDEQUE_H_
#define TDEQUE_H_

#include <string>
#include <sstream>
#include <iostream>
#include <stdexcept>

template <typename T>
class Deque {
private:
  // Instance variables
  T* container_;            // A pointer to a type T object of which queue will be comprised.
  int container_capacity_;  // How many elements the current queue can hold.
  int num_elements_;        // Number of elements currently in the queue.
  int head_index_;          // Queue's starting index.
  int tail_index_;          // Queue's ending index.
  
  // Private helper function for the push functions that doubles the size of the queue when an
  // element is pushed to a full queue. A temporary array is created that's twice the size of the
  // old one. The elements from the old array are then copied to the new one in the correct order.
  // The head index will not be negative here, so it's okay to use the mod operator with it when it
  // goes past the size of the original queue. Set the new tail index to be the last element in the
  // queue, move the head index back to the first element in the queue, delete the old array, and
  // then set it equal to the temporary one to create the new queue. The temporary array doesn't
  // have to be deleted – memory will override it next time the function is called.
  void grow() {
    T* temp_container_;
    
    try {
      // For testing out-of-memory-exception if queue becomes too large.
      // temp_container_ = new T[1000000000000000000];
      temp_container_ = new T[container_capacity_ * 2];
      
    } catch (std::bad_alloc e) {
      std::cout << "Out of memory: Cannot grow the size of the queue.\n";
      throw e;
    }
    
    for (int i = 0; i != num_elements_; ++i)
      temp_container_[i] = container_[(head_index_ + i) % container_capacity_];
    
    tail_index_ = num_elements_ - 1;
    head_index_ = 0;
    container_capacity_ *= 2;
    
    delete[] container_;
    container_ = temp_container_;
  }
  
  // Private helper function for the pop functions that decreases the size of the array by half. It
  // creates a temporary array to replace the old one and transfers its elements starting at the
  // head index incrementing up. The old queue is deleted and then replaced with the new one that's
  // half the size. The head and tail indices are then properly reset to fit the new samller queue.
  void shrink() {
    T* temp_container_ = new T[container_capacity_ / 2];
    
    for (int i = 0; i != num_elements_; ++i)
      temp_container_[i] = container_[(head_index_ + i) % container_capacity_];
    
    delete[] container_;
    container_ = temp_container_;
    container_capacity_ /= 2;
    head_index_ = 0;
    tail_index_ = num_elements_ - 1;
  }
  
  // Tells whether the queue is full or not by checking if there's an equal amount of elements
  // stored as there are capable.
  bool full() {
    return (num_elements_ == container_capacity_);
  }
  
public:
  // Constructor that is called every time an object of class Deque is created. It creates and
  // initializes a new array of 8 pointers to objects of type T, sets the capacity to 8, and
  // appropriately sets the indicies to increment correctly in subsequent functions.
  Deque() {
    container_ = new T[8];
    container_capacity_ = 8;
    num_elements_ = 0;
    head_index_ = 0;
    tail_index_ = -1;
  }
  
  // Inserts an element at the front of the queue. First check's if the queue is already full and
  // grows it as necessary, increments the number of elements since a new element is being added,
  // decrements the head and adds it to queue's capacity if its negative to allow for wrap-around
  // functionality and avoid quadratic time complexity of indexing through the array. The item is
  // then placed where the new head index is.
  void push_front(T item) {
    if (full())
      grow();
    
    // Reset the indices if the queue is empty, just in case either is off.
    if (empty()) {
      head_index_ = 0;
      tail_index_ = -1;
    }
    
    ++num_elements_;
    --head_index_;
    
    if (head_index_ < 0)
      head_index_ += container_capacity_;
    
    container_[head_index_] = item;
  }
  
  // Inserts an element at the back of the queue. First checks if the queue is already full and
  // grows it as necessary, increments the number of elements since a new element is being added,
  // increments the tail index to move up one in the queue and place the item there.
  void push_back(T item) {
    if (full())
      grow();
    
    // Reset the indices if the queue is empty, just in case either is off.
    if (empty()) {
      head_index_ = 0;
      tail_index_ = -1;
    }
    
    ++num_elements_;
    ++tail_index_;
    
    container_[tail_index_] = item;
  }
  
  // Removes and returns the element at the front of the queue. First checks if the queue is empty
  // and throws an appropriate exception that skips the pop functionality. Then checks if the
  // queue is only a quarter full and greater than 8 and appropriately calls the shrink function.
  // After checking the two conditions, the number of elements are decremented by 1 and the head
  // index is incremeneted by one since the first element is now gone. If the head element is being
  // popped off an index that exceeds the queue, its subtracted from the queue's capacity to wrap
  // back around to front of the array. The element at the original position is then returned.
  T pop_front() {
    try {
      if (empty()) {
        // Reset indices to start over.
        head_index_ = 0;
        tail_index_ = -1;
        throw std::out_of_range("");
      }
      
      if (num_elements_ == (container_capacity_) / 4 && (container_capacity_ > 8))
        shrink();
      
      --num_elements_;
      ++head_index_;
      
      if (head_index_ > container_capacity_)
        head_index_ -= container_capacity_;
      
      return container_[(head_index_ - 1)];
      
      
    } catch (std::out_of_range e) {
      std::cout << "Invalid Operation: Cannot pop from an empty queue.\n";
      throw e;
    }
  }
  
  // Removes and returns the element at the back of the queue. Checks necessary conditions to throw
  // an exception or call the shrink function, decrements the number of elements since an element is
  // getting popped off, and decrements the tail index by 1 since the element getting popped off is
  // done so from the back where the current tail index is. The tail index is added to the
  // queue's capacity if it's negative for wrap-around functionality.
  T pop_back() {
    try {
      if (empty()) {
        // Reset indices to start over.
        head_index_ = 0;
        tail_index_ = -1;
        throw std::out_of_range("");
      }
      if (num_elements_ == (container_capacity_) / 4 && (container_capacity_ > 8))
        shrink();
      
      --num_elements_;
      --tail_index_;
      
      if (tail_index_ < -1) // Since the tail index starts at -1.
        tail_index_ += container_capacity_;
      
      return container_[(tail_index_ + 1)];
      
    } catch (std::out_of_range e) {
      std::cout << "Invalid Operation: Cannot pop from an empty queue.\n";
      throw e;
    }
  }
  
  // Returns the current size of the queue.
  int size() {
    return container_capacity_;
  }
  
  // Tells whether the queue is empty or not and returns true if the number of elements is indeed 0.
  bool empty() {
    return (num_elements_ == 0);
  }
  
  // Puts the contents of the queue from the front to the back into a return string with each T
  // item followed by a new line. Iterate through the queue for how many elements are currently in
  // it and then choose the elements starting from the head index up to the last element. If the
  // head index exceeds the length of the queue, it's then modded with the queue size to wrap-around
  // back to the first index of the queue when needed. Use the \n for a new line as implied in the
  // directions to avoid possible errors from clearing the buffer with endl.
  std::string toStr() {
    std::stringstream ss;
    for (int i = 0; i != num_elements_; ++i)
      ss << container_[((head_index_ + i) % container_capacity_)] << "\n";
    return ss.str();
  }
  
  // Destructor to delete the memory allocated to the array of pointers to string objects.
  ~Deque() {
    delete[] container_;
  }
};

#endif // TDEQUE_H_
